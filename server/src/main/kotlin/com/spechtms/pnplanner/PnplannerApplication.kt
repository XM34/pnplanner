package com.spechtms.pnplanner

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PnplannerApplication

fun main(args: Array<String>) {
	runApplication<PnplannerApplication>(*args)
}
