import React, {FunctionComponent, useState} from "react";
import "../assets/styles/Markdown.scss";
import MarkdownEditor from "../components/MarkdownEditor";

const DocumentEditPage: FunctionComponent = () => {
    const [markdown, setMarkdown] = useState("");

    return (
        <MarkdownEditor classname={"w-50"} initialValue={markdown}/>
    );
};

export default DocumentEditPage;