import React, {useState} from "react";
import WithClassname from "../types/WithClassname";
import sanitize from "sanitize-html";
import {Marked} from "@ts-stack/markdown";

type MarkdownEditorProps = {
    initialValue?: string,
    onChange?: (newValue: string) => unknown
};

const MarkdownEditor = (props: MarkdownEditorProps & WithClassname) => {
    const [value, setValue] = useState(props.initialValue || "");
    const markdownRendered = sanitize(Marked.parse(value));
    const className = [props.classname, "flex items-stretch relative"].join(" ");
    const onChange = (e:string) => {
        setValue(e);
        props.onChange && props.onChange(e);
    };

    return (
        <div className={className}>
            <div className={"overflow-hidden pa3"}>
                <div className={"h-100 pa2 ba"}>
                    <div>
                        {/*Toolbar*/}
                    </div>
                    <textarea
                        className={"h-100 w-100 maxw-100 resize-x bn b--mid-gray ff-Script"}
                        value={value}
                        onChange={e => onChange(e.target.value)}
                    />
                </div>
            </div>
            <div className={"flex-auto relative pa3"}>
                <div
                    className={"h-100 w-100 pa2 ba overflow-hidden markdown"}
                    dangerouslySetInnerHTML={{__html: markdownRendered}}
                />
            </div>
        </div>

    );
};

export default MarkdownEditor;