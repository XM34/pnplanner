import React from 'react';
import './assets/styles/App.scss';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import OverviewPage from "./pages/OverviewPage";
import DocumentEditPage from "./pages/DocumentEditPage";

function App() {
  return (
    <Router>
      <Switch>
        <Route path={"/"}
               exact={true}
               render={(props) => <OverviewPage/>}/>
        <Route path={"/edit"}
               exact={false}
               render={(props) => <DocumentEditPage/>}/>
      </Switch>
    </Router>
  );
}

export default App;
