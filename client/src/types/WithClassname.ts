type WithClassname = {
    classname?: string
};

export default WithClassname;